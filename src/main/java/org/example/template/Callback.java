package org.example.template;

import javax.persistence.EntityManager;

public interface Callback<T> {
    T execute(final EntityManager em);
}
