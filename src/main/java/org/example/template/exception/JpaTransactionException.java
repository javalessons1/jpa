package org.example.template.exception;

public class JpaTransactionException extends RuntimeException {
    public JpaTransactionException() {
    }

    public JpaTransactionException(String message) {
        super(message);
    }

    public JpaTransactionException(String message, Throwable cause) {
        super(message, cause);
    }

    public JpaTransactionException(Throwable cause) {
        super(cause);
    }

}
