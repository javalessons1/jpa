package org.example.repository;

import java.util.List;
import java.util.Optional;

public interface CrudRepository<ENTITY, ID> {
  List<ENTITY> getAll(int limit, int offset);
  Optional<ENTITY> getById(ID id);
  ENTITY save(ENTITY entity);
  List<ENTITY> saveAll(Iterable<ENTITY> entities);
  void deleteById(ID id);
  void deleteAllById(Iterable<ID> ids);
  ENTITY getByLogin(final String login);
}
