package org.example.repository;

import org.example.repository.exception.NoIdException;
import org.example.repository.exception.ReflectionException;
import org.example.template.JpaTransactionTemplate;

import javax.persistence.Id;
import java.lang.reflect.Field;
import java.util.*;

class CrudRepositoryImpl<ENTITY, ID> implements CrudRepository<ENTITY, ID> {
  private final JpaTransactionTemplate template;
  private final Class<ENTITY> entityClazz;
  private final Field id;

  public CrudRepositoryImpl(final JpaTransactionTemplate template, final Class<ENTITY> entityClazz) {
    this.template = template;
    this.entityClazz = entityClazz;
    this.id = Arrays.stream(entityClazz.getDeclaredFields())
        .filter(o -> o.isAnnotationPresent(Id.class))
        .findFirst()
        .orElseThrow(NoIdException::new);

    init();
  }

  private void init() {
    id.setAccessible(true);
  }

  @Override
  public List<ENTITY> getAll(final int limit, final int offset) {
    return template.executeInTransaction(em -> em.createQuery(
                "SELECT e FROM " + entityClazz.getSimpleName() + " e",
                entityClazz
            )
            .setFirstResult(0) // offset
            .setMaxResults(50) // limit
            .getResultList()
    );
  }

  @Override
  public Optional<ENTITY> getById(ID id) {
    return template.executeInTransaction(em -> Optional.ofNullable(em.find(entityClazz, id)));
  }

  @Override
  public ENTITY save(ENTITY entity) {
    return template.executeInTransaction(em -> {
      try {
        final Object id = this.id.get(entity);
        if (id == null) {
          em.persist(entity);
          return entity;
        }
        return em.merge(entity);
      } catch (IllegalAccessException e) {
        throw new ReflectionException(e);
      }
    });
  }

  @Override
  public List<ENTITY> saveAll(final Iterable<ENTITY> entities) {
    return template.executeInTransaction(em -> {
      final List<ENTITY> result = new ArrayList<>();
      final Iterator<ENTITY> iterator = entities.iterator();
      while (iterator.hasNext()) {
        final ENTITY entity = iterator.next();
        final ENTITY saved = save(entity);
        result.add(saved);
      }
      return Collections.unmodifiableList(result);
    });
  }

  @Override
  public void deleteById(final ID id) {
    template.executeInTransaction(em -> {
      final ENTITY reference = em.getReference(entityClazz, id);
      em.remove(reference);
      return null;
    });
  }

  @Override
  public void deleteAllById(final Iterable<ID> ids) {
    template.executeInTransaction(em -> {
      final Iterator<ID> iterator = ids.iterator();
      while (iterator.hasNext()) {
        final ID id = iterator.next();
        deleteById(id);
      }
      return null;
    });
  }
  
  @Override
  public ENTITY getByLogin(final String login) {
    return template.executeInTransaction(em -> em.createQuery(
                    "SELECT e FROM " + entityClazz.getSimpleName() + " e WHERE e.login=:login",
                    entityClazz
            )
            .setParameter("login",login).getSingleResult());
  }
}
