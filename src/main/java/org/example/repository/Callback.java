package org.example.repository;

import javax.persistence.EntityManager;

interface Callback<T> {
  T execute(final EntityManager em);
}
