package org.example.repository.exception;

public class BadEntityType extends RuntimeException {
  public BadEntityType(String message) {
    super(message);
  }

}
