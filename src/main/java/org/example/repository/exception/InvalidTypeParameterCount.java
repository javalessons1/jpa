package org.example.repository.exception;

public class InvalidTypeParameterCount extends RuntimeException {
  public InvalidTypeParameterCount() {
  }

  public InvalidTypeParameterCount(String message) {
    super(message);
  }

  public InvalidTypeParameterCount(String message, Throwable cause) {
    super(message, cause);
  }

  public InvalidTypeParameterCount(Throwable cause) {
    super(cause);
  }

  public InvalidTypeParameterCount(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
